angular.module('starter', ['ionic', 'ngCordova', 'starter.controllers', 'ion-floating-menu'])

.run(function ($ionicPlatform, $cordovaSQLite, $cordovaStatusbar, $rootScope, $ionicLoading, $cordovaDevice) {

    $ionicPlatform.ready(function () {
      
 setTimeout(function() {
        navigator.splashscreen.hide();
    }, 300);

      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);
      }
      if (window.StatusBar) {
        StatusBar.styleDefault();
      }


      //$cordovaSQLite.deleteDB("mydb32.db");


      $cordovaStatusbar.styleHex('#054D44');



      $rootScope.show = function () {
        $ionicLoading.show({
          template: '<ion-spinner></ion-spinner>',
          duration: 3000
        }).then(function () {
          console.log("The loading indicator is now displayed");
        });
      };
      $rootScope.hide = function () {
        $ionicLoading.hide().then(function () {
          console.log("The loading indicator is now hidden");
        });
      };


      /*  var db;
        db = window.sqlitePlugin.openDatabase({
          name: "mydb32.db",
          location: 'default'
        });
        
        db.transaction(function (tx) {
          tx.executeSql('CREATE TABLE IF NOT EXISTS lists (id text, date text, qty text)');
          tx.executeSql("CREATE TABLE IF NOT EXISTS products (id text, list text, ean text, sap text, unidad text, name text, price text, priceO text, priceM text, date text)");
          tx.executeSql("CREATE TABLE IF NOT EXISTS ubicaciones (id text, name text)");
          tx.executeSql("CREATE TABLE IF NOT EXISTS auditorias (id text, location text, list text, date text)");
          tx.executeSql("CREATE TABLE IF NOT EXISTS data (id text, list text, location text, ean text, sap text, unidad text, name text, price text, priceO text, priceM text, date text)");
          tx.executeSql("CREATE TABLE IF NOT EXISTS settings (opcion text, valor text)");

        }, function (tx, e) {
          console.log("ERROR: " + e.message);

        });*/


      //db = $cordovaSQLite.openDB("mydb32.db");
      /*debugger;
        $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS lists (id text, date text, qty text)").then(function(res) {
                   console.log(query);
                 }, function(err) {
                   alert("Error al crear listas");
                   console.error(err);
                 });*/



    });

  })
  .config(function ($httpProvider) {
    $httpProvider.defaults.timeout = 5000;
    $httpProvider.interceptors.push(function ($rootScope) {
      return {
        request: function (config) {
          //    $rootScope.show();
          $rootScope.$broadcast('loading:show')
          return config
        },
        response: function (response) {
          //    $rootScope.hide();
          $rootScope.$broadcast('loading:hide')
          return response
        }
      }
    })
  })
