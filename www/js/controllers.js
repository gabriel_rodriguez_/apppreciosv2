angular.module('starter.controllers', [])
  .controller('mainController', function ($scope, $cordovaBarcodeScanner, $http, $cordovaSQLite, $timeout, $ionicPopup, $rootScope, $ionicLoading, $cordovaDevice, $cordovaFile) {

    $rootScope.$on('loading:show', function () {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>',
      })
    });

    $rootScope.$on('loading:hide', function () {
      $ionicLoading.hide()
    });
    // INICIALIZO LA APP
    // ********************************************************************
    $scope.init = function ($apiPath) {

      $scope.isActive = false;
      $scope.showLogin = false;
      $scope.username = "";
      $scope.password = "";
      $scope.exMenu = false;
      $scope.loginMessage = "";
      $scope.showLists = false;
      $scope.showProducts = false;
      $scope.showDate = false;

      $scope.listDate = "";
      $scope.menuTitle = "Listas";
      $scope.itemsLength = 0;
      $scope.productsLength = 0;
      $scope.listId = 0;
      $scope.locationId = 0;
      $scope.productEAN = 0;
      $scope.listQty = 0;
      $scope.pvpnew = "";
      $scope.ponew = "";
      $scope.pmnew = "";
      $scope.locationName = "";
      $scope.name = "";
      $scope.barCode = "";
      $scope.pname = "";
      $scope.actualScan = "";

      $scope.user = "";
      $scope.password = "";
      //http://siberian.com.ec/api/
      //http://10.11.7.43/api/
      $scope.apiPath = 'http://siberian.com.ec/api/'

      $scope.products = [];
      $scope.lists = [];
      $scope.product = [];
      $scope.productName = [];
      $scope.productPrice = [];
      $scope.response1 = [];
      $scope.response2 = [];
      $scope.actualList = [];

      $scope.isNewAudit = false;
      $scope.showList = true;
      $scope.isEdit = false;
      $scope.isNewPvP = false;
      $scope.backBotton = false;
      $scope.notFoundA = false;
      $scope.notFoundP = false;
      $scope.notFoundL = false;
      $scope.notFoundU = false;
      $scope.notFoundAPI = false;

      $scope.auxAudit = null;
      $scope.local = true;
      $scope.actualUser = "";

      $scope.url = "";
      if (ionic.Platform.isAndroid()) {
        $scope.url = "/android_asset/www/";
      }

      $scope.loadLogin();

    };


    $scope.fileController = function () {

      $cordovaFile.getFreeDiskSpace()
        .then(function (success) {
          console.log("getFreeDiskSpace");
          // success in kilobytes
        }, function (error) {
          console.log("getFreeDiskSpace " + error);
          // error
        });


      // CHECK
      $cordovaFile.checkDir(cordova.file.dataDirectory, "appdata")
        .then(function (success) {
          console.log("Success checkDir");

          // success
        }, function (error) {
          console.log("checkDir " + error);

          // error
        });


      $cordovaFile.checkFile(cordova.file.dataDirectory, "appdata/file.json")
        .then(function (success) {
          console.log("Success checkFile");

          // success
        }, function (error) {
          console.log("checkFile " + error);

          // error
        });


      // CREATE
      $cordovaFile.createDir(cordova.file.dataDirectory, "appdata", false)
        .then(function (success) {
          console.log("Success createDir");

          // success
        }, function (error) {
          console.log("createDir " + error);

          // error
        });

      $cordovaFile.createFile(cordova.file.dataDirectory, "file.json", true)
        .then(function (success) {
          console.log("Success createFile");

          // success
        }, function (error) {
          console.log("createFile " + error);

          // error
        });
    };


    $scope.read_text = function () {

      // READ
      $scope.read = "";
      $cordovaFile.readAsText(cordova.file.dataDirectory, "appdata/file.json")
        .then(function (success) {
          $scope.read = success;
          //   $scope.lists = angular.toJson($scope.read);
          //  console.log($scope.lists);
          //    console.log(" ************** ");
          $scope.lists = angular.fromJson($scope.read);
          console.log($scope.lists);

          console.log("Success readAsText");

          // success
        }, function (error) {
          console.log("Error readAsText");

          // error
        });

    }


    $scope.write_File = function (text) {

      // WRITE
      $cordovaFile.writeFile(cordova.file.dataDirectory, "appdata/file.json", text, true)
        .then(function (success) {
          console.log("Success writing file.." + success);
          $scope.read_text();
          // success
        }, function (error) {
          console.log("Error writing file.." + error);
          $scope.read_text();
        });

      /*
            $cordovaFile.writeExistingFile(cordova.file.dataDirectory, "appdata/file.json", text)
              .then(function (success) {
                alert("Success writing file.." + success);
                $scope.read_text();
                // success
              }, function (error) {
                alert("Error writing file.." + error);
                // error
              });
      */
    };

    // ********************************************************************
    // CONFIGURACIONES
    // ********************************************************************


    // Resetea la APP
    $scope.resetapp = function () {
      $scope.reset();
      $scope.init();
    }

    // Limpia las ventanas
    $scope.reset = function () {
      $scope.showDate = false;
      $scope.notFoundP = false;
      $scope.notFoundA = false;
      $scope.notFoundL = false;
      $scope.notFoundU = false;
      $scope.showLists = false;
      $scope.showProducts = false;
      $scope.isNewAudit = false;
      $scope.isEdit = false;
      $scope.isNewPvP = false;
      $scope.pvpinput = "";
      $scope.pvpnew = "";
      $scope.ponew = "";
      $scope.pmnew = "";
      $scope.exMenu = false;
      $scope.showLogin = false;
      $scope.isNewProduct = false;

    };


    $scope.openExMenu = function () {
      console.log("click menu");
      $scope.exMenu = !$scope.exMenu;
    };


    $scope.closeMenu = function () {
      $scope.exMenu = false;

    }


    $scope.loadLogin = function () {
      $scope.reset();
      $scope.showLogin = true;
      $scope.backButton = false;
      $scope.menuTitle = "Login";
    };




    $scope.login = function (username, password) {


      var url = "http://siberian.com.ec/api/";
      $scope.user = username.toLowerCase();
      $scope.password = password;
      $scope.actualDate = $scope.formatDate();
      var request = $http.post(url + "login", "username=" + $scope.user + "&password=" + $scope.password, {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      }).success(function (data) {
        if (data != "error") {


          var query = "INSERT INTO login (username, password,active) VALUES (?,?,?)";
          $cordovaSQLite.execute($scope.db, query, [$scope.user, $scope.password, "1"]).then(function (res) {
            console.log("insertId: " + res.insertId);
          }, function (err) {
            console.error(err);
          });


          $scope.write_File(data);



          /*
           $http.get('appdata/file.json')
                  .success(function (data) {
                      // The json data will now be in scope.
                      $scope.myJsonData = data;
                  });


          */

          console.log($scope.lists);

          //$scope.lists = 
          console.log("Sesión iniciada");
          $scope.loadLists();

        } else {
          $scope.loginMessage = "Usuario o contraseña erróneos";
        }


      }).error(function (data, status) {


        $cordovaSQLite.execute($scope.db, "SELECT * FROM login ORDER BY id DESC LIMIT 1;", []).then(function (result) {
          if (result.rows.length > 0) {

            for (var i = 0; i < result.rows.length; i++) {
              //  $scope.results.push(result.rows[i]);
              console.log("Active :" + result.rows.item(i).active);
              if (result.rows.item(i).active == 1 && result.rows.item(i).username == $scope.user && result.rows.item(i).password == $scope.password) {

                $scope.isActive = true;

                $scope.read_text();
                console.log("Sesión iniciada offline");
                $scope.loadLists();

              } else {

                // $scope.showAlert("Iniciar sesión primero en la base..");

              }

            }

          } else {
            //   $scope.showAlert("Iniciar sesión primero en la base..");
            console.log("####console######## NO results found #######" + "Table record #: ");
          }
        }, function (error) {
          console.log(error);
        })



        console.log("Inicio de sesión fallido");
        if (status == "404") {
          $scope.loginMessage = "No hay conexión";
        } else {
          // $scope.loginMessage = status;
        }


        $ionicLoading.hide();
        console.error('Login error', status, data);
      });

    }


    /////////////////////////////////////
    ///////Authenticate
    ////////////////////////////////////
    $scope.authenticate = function (username, password) {

      if ($scope.isActive) {
        $scope.read_text();
        console.log("Sesión iniciada offline");
        $scope.loadLists();

      } else {

        $scope.login(username, password);

      }
    };

    $scope.loadCloseSession = function () {
      $scope.resetapp();
    };

    $scope.loadConfigurations = function () {
      alert("load configurations");
    };

    // Sincroniza la base de datos
    $scope.sycDB = function () {


    };
    // *******************************************************************************
    //  LISTAS
    // *******************************************************************************

    // Prepara la ventana de listas
    $scope.loadLists = function () {
      $scope.reset();
      //  $scope.getLists();
      //  $scope.products = [];
      $scope.showLists = true;
      $scope.showLogin = false;
      $scope.backButton = false;
      $scope.menuTitle = "Listas";
    }


    //Solicita las Listas
    $scope.getLists = function () {
      $scope.authenticate($scope.user, $scope.password);
    };

    // *******************************************************************************
    //  PRODUCTOS
    // *******************************************************************************

    // Prepara la ventana de Productos
    $scope.loadProducts = function (list) {

      if (list != null) {
        $scope.actualList = list;
      }
      $scope.showList = true;
      // $scope.reset();
      $scope.getProducts($scope.actualList);
      $scope.showList = true;
      $scope.showProducts = true;
      $scope.backButton = true;
      $scope.isEdit = false;
      $scope.isNewPvP = false;
      $scope.isNewProduct = false;

      $scope.menuTitle = "Productos";
      $scope.showDate = true;
    };

    $scope.getProducts = function (list) {
      $scope.showLists = false;
      $scope.showLogin = false;
      //add new product


      $cordovaSQLite.execute($scope.db, "SELECT * FROM product where  actualListId = ?;", [list.id]).then(function (result) {
        if (result.rows.length > 0) {

          for (var i = 0; i < result.rows.length; i++) {
            $scope._ean = result.rows.item(i).ean;
            $scope._name = result.rows.item(i).name;
            $scope._price = result.rows.item(i).price;
            $scope._priceO = result.rows.item(i).price_o;
            $scope._priceM = result.rows.item(i).price_m;
            $scope._actualListId = result.rows.item(i).actualListId;

            var bool = 1;
            angular.forEach(list.products, function (value, key) {
              if (value.ean != result.rows.item(i).ean) {
                bool = bool * 1;
              } else {
                bool = bool * 0;
              }

            });

            if (bool == 1) {
              list.products.push(result.rows.item(i));
            }

          }
        }

      });

      $scope.products = list.products;
    }


    $scope.createPriceOff = function (ean, list, price, priceO, priceM,status) {
      console.log("LOCAL: Actualizando precio ");
      $scope._ean = ean;
      $scope._list = list;
      $scope._price = price;
      $scope._priceO = priceO;
      $scope._priceM = priceM;

       $scope._status = status;


if(status=="create"){


      var query = "INSERT INTO list (productId, listId,price,priceO,priceM,status) VALUES (?,?,?,?,?,?)";
      $cordovaSQLite.execute($scope.db, query, [$scope._ean, $scope._list, $scope._price, $scope._priceO, $scope._priceM, $scope._status]).then(function (res) {
        console.log("insertId: " + res.insertId);
      }, function (err) {
        console.error(err);
      });


}


if(status=="update"){

   var query = "UPDATE list set price=? ,priceO=? ,priceM=? ,status=?) VALUES (?,?,?,?) WHERE productId = ? and listId = ?";
      console.log(query);
      $cordovaSQLite.execute($scope.db, query, [$scope._price, $scope._priceO, $scope._priceM, $scope._status,$scope._ean, $scope._list]).then(function (res) {
        console.log("insertId: " + res.insertId);
        //  $scope.createPvP(pvp, pvo, pvm);
      }, function (err) {
        console.error(err);
      });
}





    };


    $scope.createPrice = function () {
      console.log("LOCAL: Actualizando precio ");
      $scope._ean = "";
      $scope._list = "";
      $scope._price = "";
      $scope._priceO = "";
      $scope._priceM = "";


      var url = "http://siberian.com.ec/api/";

      $cordovaSQLite.execute($scope.db, "SELECT * FROM list;", []).then(function (result) {
        if (result.rows.length > 0) {

          for (var i = 0; i < result.rows.length; i++) {
            $scope._ean = result.rows.item(i).productId;
            $scope._list = result.rows.item(i).listId;
            $scope._price = result.rows.item(i).price;
            $scope._priceO = result.rows.item(i).priceO;
            $scope._priceM = result.rows.item(i).priceM;


            var request = $http.post(url + "list", "productId=" + $scope._ean + "&listId=" + $scope._list + "&price=" + $scope._price + "&priceO=" + $scope._priceO + "&priceM=" + $scope._priceM, {
              headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
              }
            }).success(function (data) {
              if (data != 0) {

                console.log("Producto con EAN :" + $scope._ean + " fue sincronizado");

                //Delete local element

                $cordovaSQLite.execute($scope.db, "DELETE from list where productId = ?;", [$scope._ean]).then(function (res) {
                  $scope.showAlert("Precio del producto cpn EAN :" + $scope._ean + " fue sincronizado");
                  console.log("Producto con EAN :" + $scope._ean + " fue eliminado localmente " + res);
                }, function (err) {

                  console.error(err);
                });

                // $scope.loadProducts();
              } else {

              }

            }).error(function (data, status) {

              $scope.showAlert("No hay conexión con la red");

              $ionicLoading.hide();

            });

          }

        } else {
          $scope.showAlert("No hay listas a actualizar..");
          console.log("####console######## NO results found #######" + "Table record #: ");
        }
      }, function (error) {
        console.log(error);
      })

      return true;

    };


    $scope.updatePrice = function (list, ean, price, priceO, priceM) {

    $scope.createPriceOff(ean, list,price, priceO, priceM,"update");

    };


    //Agregar precio
    $scope.addPvP = function () {
      $scope.isNewPvP = true;
      $scope.isNewAudit = false;
      $scope.isEdit = false;
      //  $scope.pvpnew="";
      $scope.showLists = false;
      $scope.showProducts = false;
      $scope.backButton = true;
      $scope.menuTitle = "Datos";
      $scope.showDate = false;
      $scope.showLocations = false;
      $scope.showList = false;
    };


    $scope.createPvP = function (pvp, pvo, pvm) {
      var ean = "";
      //  if ($scope.product.ean == 0) {
      ean = $scope.actualScan;
      //  } else {
      //    ean = $scope.product.ean;
      //   }
      $scope.createPriceOff(ean, $scope.actualList.id, pvp, pvo, pvm,"create");
      $scope.pvpnew = pvp;
      $scope.ponew = pvo;
      $scope.pmnew = pvm;
      $scope.isNewProduct = false;
      $scope.loadProducts($scope.actualList);
    };


    $scope.createProductOff = function (name, pvp, pvo, pvm) {

      var actualListId = $scope.actualList.id;
      var ean = $scope.actualScan;
      console.log("LOCAL: Registrando nuevo producto ");

      $scope._ean = ean;
      $scope._name = name;
      $scope._price = pvp;
      $scope._priceO = pvo;
      $scope._priceM = pvm;


      var query = "INSERT INTO product (ean, name,price,price_o,price_m,actualListId) VALUES (?,?,?,?,?,?)";
      $cordovaSQLite.execute($scope.db, query, [$scope._ean, $scope._name, $scope._price, $scope._priceO, $scope._priceM, actualListId]).then(function (res) {
        console.log("insertId: " + res.insertId);
        $scope.createPvP(pvp, pvo, pvm);
      }, function (err) {
        console.error(err);
      });

    };


    //INSERT INTO `products`(`id`, `name`, `ean`, `sap`, `unit`, `price`, `price_o`, `price_m`, `createdDate`) VALUES 
    $scope.createProduct = function (name, pvp, pvo, pvm) {

      //  var actualListId = $scope.actualList.id;
      var ean = $scope.actualScan;
      console.log("LOCAL: Registrando nuevo producto ");

      $scope._ean = ean;
      $scope._name = name;
      $scope._price = pvp;
      $scope._priceO = pvo;
      $scope._priceM = pvm;



      var url = "http://siberian.com.ec/api/";

      $cordovaSQLite.execute($scope.db, "SELECT * FROM product;", []).then(function (result) {
        if (result.rows.length > 0) {

          for (var i = 0; i < result.rows.length; i++) {
            $scope._ean = result.rows.item(i).ean;
            $scope._name = result.rows.item(i).name;
            $scope._price = result.rows.item(i).price;
            $scope._priceO = result.rows.item(i).price_o;
            $scope._priceM = result.rows.item(i).price_m;
            $scope._actualListId = result.rows.item(i).actualListId;



            var request = $http.post(url + "product", "name=" + $scope._name + "&ean=" + $scope._ean + "&price=" + $scope._price + "&price_o=" + $scope._priceO + "&price_m=" + $scope._priceM + "&actualListId=" + $scope._actualListId, {
              headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
              }
            }).success(function (data) {
              if (data != 0) {

                console.log("Producto con EAN :" + $scope._ean + " fue sincronizado");

                //Delete local element
                $cordovaSQLite.execute($scope.db, "DELETE  from product where ean = ?;", [$scope._ean]).then(function (res) {
                  console.log("Producto nuevo con EAN :" + $scope._ean + " fue eliminado localmente");
                  $scope.showAlert("Producto " + $scope._name + " sincronizado correctamente!");
                }, function (err) {

                  console.error(err);
                });

                // $scope.loadProducts();
              } else {

              }

            }).error(function (data, status) {
              $scope.showAlert("No hay conexión con la red");
              $ionicLoading.hide();

            });

          }

        } else {
          $scope.showAlert("No hay productos para sincronizar");
          console.log("####console######## NO results found #######" + "Table product #: ");
        }
      }, function (error) {
        console.log(error);
      })

      return true;
    };

    $scope.editPVP = function (pvp, pvo, pvm) {

      console.log(pvp + " " + pvo + " " + pvm);
      $scope.updatePrice($scope.actualList.id, $scope.product.ean, pvp, pvo, pvm);
      $scope.pvpnew = pvp;
      $scope.ponew = pvo;
      $scope.pmnew = pvm;
      $scope.loadProducts($scope.actualList);
    };

    //Editar Precio
    $scope.addEdit = function (product) {
      console.log(product.price);
      $scope.isEdit = true;
      $scope.isNewPvP = false;
      $scope.isNewAudit = false;
      $scope.showLists = false;
      $scope.showProducts = false;
      $scope.backButton = true;
      $scope.menuTitle = "Editar";
      $scope.showDate = false;
      $scope.showLocations = false;
      $scope.showList = false;
      $scope.product = product;
      $scope.pvpnew = parseFloat(product.price);
      $scope.ponew = parseFloat(product.price_o);
      $scope.pmnew = parseFloat(product.price_m);

    };


    //   FUNCIONES VARIAS
    // *******************************************************************************
    /*  $scope.doRefresh = function () {
        if ($scope.showList) {
          $timeout(function () {
            // $scope.loadLists();
            // $scope.sycDB();
            $scope.upload();
            $scope.$broadcast('scroll.refreshComplete');
          }, 1000);
        };
      };*/


    //***********************************************
    //***** Upload work 
    //**********************************************


    $scope.upload = function () {
      // $ionicLoading.show();
      //  $scope.createPrice();
      //  $scope.createProduct();

      if ($scope.createPrice() && $scope.createProduct()) {

        $timeout(function () {
          $scope.login($scope.user, $scope.password);

        }, 5000);



      }

      // $ionicLoading.hide();
    }

    $scope.showAlert = function (mensaje) {
      var alertPopup = $ionicPopup.alert({
        title: 'Mensaje',
        template: "<center>" + mensaje + "</center>"
      });
      alertPopup.then(function (res) {});
    };

    /* Funcion para subir los datos */
    $scope.syc = function (list) {

      $scope.response1 = [];
      $scope.response2 = [];

      var url = $scope.apiPath;
      $http.post(url + "syc", "auditorias=" + angular.toJson($scope.response1) + "&data=" + angular.toJson($scope.response2) + "&precios=" + angular.toJson($scope.response3), {
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }
        }).success(function (data) {
          $scope.showAlert("Lista subida exitosamente..");
          console.log("Lista subida al API");
        })
        .error(function (data, status) {
          console.log(status);
        });

    };


    $scope.showConfirm = function ($location) {
      var bool = true;
      angular.forEach($scope.audits, function (audit) {
        if (audit.id == $location.id) {
          $scope.showAlert("Esta opción ya fue escogida");
          bool = false;
        };
      });

      if (bool) {
        audit = [];
        $scope.addAudit($location.id, $scope.listId);
        audit.id = $location.id;
      };
    };

    // A confirm dialog
    $scope.showConfirmProduct = function () {
      var confirmPopup = $ionicPopup.confirm({
        title: 'Producto nuevo encontrado',
        template: '¿Desea guardar el producto?'
      });
      confirmPopup.then(function (res) {
        if (res) {
          console.log('Si');

          $scope.isNewPvP = false;
          $scope.showProducts = false;
          $scope.isNewProduct = true;

          // $scope.createProduct(name,ean, pvp, pvo, pvm, actualListId);


        } else {
          console.log('No');
        }
      });
    };


    $scope.compare = function (imageData, product) {
      $scope.actualScan = imageData.text;
      if (imageData.text == product.ean) {
        $scope.product = product;
        $scope.reset();
        $scope.showProducts = false;
        $scope.isNewPvP = true;
        $scope.menuTitle = "Precio";
        // console.log($scope);
      } else {
        if (imageData.text != "") {
          $scope.showConfirmProduct();
          //LoadNewProduct
        }

      }

    }

    /* Funcion escanear codigo de barras */
    $scope.scanBarcode = function (product) {

      $cordovaBarcodeScanner.scan().then(function (imageData) {

          $scope.actualScan = imageData.text;

          if (product != null) {

            $scope.compare(imageData, product);

          } else {

            var bool = false;
            angular.forEach($scope.products, function (value, key) {

              if (value.ean == imageData.text) {
                $scope.compare(imageData, value);

              } else {

                bool = true;
              }

            });

            if (bool) {
              if (imageData.text != "") {
                $scope.showConfirmProduct();
                //LoadNewProduct
              }
            }

          }

        },
        function (error) {
          console.log("An error happened -> " + error);
        });
    };

    $scope.loadBack = function () {
      /*  if ($scope.showAudits) {
          $scope.reset();
          $scope.loadLists();
        };*/
      /*  if ($scope.isNewAudit) {
          $scope.reset();
          $scope.loadAudits();
        };*/
      if ($scope.showProducts) {
        $scope.reset();
        $scope.loadLists($scope.lists);
      };
      if ($scope.isEdit) {
        $scope.reset();
        $scope.loadProducts($scope.actualList);
      }
      if ($scope.isNewProduct) {
        $scope.reset();
        $scope.loadProducts($scope.actualList);
      }

      /*   if ($scope.isNewPvP) {
           $scope.reset();
           $scope.loadProducts();
         };*/
    };

    $scope.formatDate = function () {
      $scope.date = new Date();
      $scope.day = $scope.date.getDate().toString();
      $scope.month = $scope.date.getMonth() + 1;
      $scope.month = $scope.month.toString();
      if ($scope.month.length == 1) {
        $scope.month = "0" + $scope.month;
      }
      if ($scope.day.length == 1) {
        $scope.day = "0" + $scope.day;
      }
      $scope.year = $scope.date.getFullYear().toString();
      $scope.fDate = $scope.day + "/" + $scope.month + "/" + $scope.year;

      return $scope.fDate;
    };

    // *******************************************************************************

    //$scope.init();
    $scope.showLogin = true;
    $scope.menuTitle = "Login";



    document.addEventListener("deviceready", onDeviceReady, false);

    function onDeviceReady() {

      $scope.fileController();

      $scope.db = null;
      //   console.log($cordovaSQLite);

      if ($cordovaDevice.getPlatform() == "Android") {
        // Works on android but not in iOS
        $scope.db = $cordovaSQLite.openDB({
          name: "mydb32.db",
          iosDatabaseLocation: 'default'
        });
      } else {
        // Works on iOS 
        $scope.db = window.sqlitePlugin.openDatabase({
          name: "mydb32.db",
          location: 2,
          createFromLocation: 1
        });
      }

      
            $cordovaSQLite.execute($scope.db, "drop table list;").then(function (res) {
              //   console.log(query);
            }, function (err) {

              console.error(err);
            });

      $cordovaSQLite.execute($scope.db, "CREATE TABLE IF NOT EXISTS product (id INTEGER PRIMARY KEY  AUTOINCREMENT, name text, ean text, price text, price_o text, price_m text,actualListId text)").then(function (res) {
        //   console.log(query);
      }, function (err) {

        console.error(err);
      });


      $cordovaSQLite.execute($scope.db, "CREATE TABLE IF NOT EXISTS list (id INTEGER PRIMARY KEY  AUTOINCREMENT, productId text, listId text, price text, priceO text, priceM text, status text)").then(function (res) {
        //   console.log(query);
      }, function (err) {

        console.error(err);
      });


      $cordovaSQLite.execute($scope.db, "CREATE TABLE IF NOT EXISTS login (id INTEGER PRIMARY KEY  AUTOINCREMENT, username text, password text, active text)").then(function (res) {
        //   console.log(query);
      }, function (err) {

        console.error(err);
      });
/*
      $cordovaSQLite.execute($scope.db, "CREATE TABLE IF NOT EXISTS price (id INTEGER PRIMARY KEY  AUTOINCREMENT, username text, password text, active text)").then(function (res) {
        //   console.log(query);
      }, function (err) {

        console.error(err);
      });*/


      $scope.init();
      //$scope.addAudit("1","14");
      // $scope.sycDB();


    }


  }).directive('decimal', function () {
    return {
      require: 'ngModel',
      link: function (scope, element, attrs, ngModelController) {

        ngModelController.$formatters.push(function (data) {
          var newData = data;

          return newData; //converted
        });
      }
    };
  });
