@echo off
echo Firmando app
jarsigner -sigalg SHA1withRSA -digestalg SHA1 -keystore app.keystore platforms\android\build\outputs\apk\android-release-unsigned.apk listapp  -tsa http://timestamp.digicert.com
echo Empaquetando app
zipalign -v 4 platforms\android\build\outputs\apk\android-release-unsigned.apk  listapp-usuario.apk
